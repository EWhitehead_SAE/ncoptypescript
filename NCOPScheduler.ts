//Wrapper for a 'unit'
class Unit 
{
    public priority: number;
    public facilitators: string[];
    public code: string;
    //constructor(public _priority: number, public _code: string, public _wrangler: string, public _melbourne: string, public _sydney: string, public _brisbane: string, public _perth: string, public _byron: string, public _adelaide: string) 
    constructor(public _priority: number, public _code: string, public _facilitators : string[]) 
    {
        //generate from csv or from inputs in the web page
        //sort the wrangler from the other inputs or split to seperate field
        this.priority = _priority;
        this.code = _code;
        this.facilitators = _facilitators;
    }
}

let spaces: number; //y values
let timeslots: number; //x values
let units: Unit[]; //pool of all units

let bestSchedule: Unit[][]; //final output
let bestPoints: number = -1000; //the amount of points the 'best schedule' has
let workingSchedule: Unit[][]; //schedule being generated
let iterations : number = 1000; //how many times we'll try
let unitNumber : number = 1;

function Run()
{
    Import();
    return false;
}

//Get data to create pool of units
function Import()
{
    //reset in case it's been run already
    bestPoints = 0;

    //grab the data from the form
    //const form = document.querySelector('form')
    //const data = new FormData(form);

    units = [];

    //console.log(unitNumber);
    //console.log((<HTMLInputElement>document.getElementById("Code"+1)).value.toString());

    //Go through all 10 unit rows and pull values from the form to populate units
    for(let i = 1; i < unitNumber; i++)
    {
        //only add them if they're not the default code
        if((<HTMLInputElement>document.getElementById("Code"+i)).value.toString() != "AAA000")
        {
            let Fid : string[] = ["Wrangler"+i,"MEL"+i,"SYD"+i,"BRS"+i,"PER"+i,"BBY"+i,"DUB"+i]//,"ADL"+i]

            let f : string[] = [(<HTMLInputElement>document.getElementById(Fid[0])).value.toString(),
            (<HTMLInputElement>document.getElementById(Fid[1])).value.toString(),
            (<HTMLInputElement>document.getElementById(Fid[2])).value.toString(),
            (<HTMLInputElement>document.getElementById(Fid[3])).value.toString(),
            (<HTMLInputElement>document.getElementById(Fid[4])).value.toString(),
            (<HTMLInputElement>document.getElementById(Fid[5])).value.toString(),
            (<HTMLInputElement>document.getElementById(Fid[6])).value.toString()]
            //(<HTMLInputElement>document.getElementById(Fid[7])).value.toString()]

            units.push(new Unit(0,(<HTMLInputElement>document.getElementById("Code"+i)).value.toString(),f));
        }
        
        //console.log(Fid)
    }

    timeslots = Number((<HTMLInputElement> document.getElementById("Timeslots")).value);
    spaces = Number((<HTMLInputElement> document.getElementById("Spaces")).value);
    iterations = Number((<HTMLInputElement> document.getElementById("Iterations")).value);
    
    //temporary debug manual fill
    /*let s : string[] = ["Zhia Zariko","Adam Ho","Aaron Williams","Peter Zhao","na","na"];
    units = [new Unit(1,"GIM110",s),
    new Unit(1,"GAD170",s),
    new Unit(1,"GAD170",s)
    ];*/

    //console.log(units);

    Populate();
}

//Fill the working schedule at random
function Populate()
{
    //initialise best schedule
    bestSchedule = new Array(timeslots);
    for(let j = 0; j < timeslots; j++)
    {
        bestSchedule[j] = new Array(spaces);
    }
    //bestSchedule = new Array<Array<Unit>>(); (alternative approach, doesn't set size)

    //run through iterations and test random schedules
    for(let i = 0; i < iterations; i++)
    {
        //workingSchedule = [new Array<Unit>(spaces),new Array<Unit>(timeslots)];
        workingSchedule = new Array(timeslots);
        for(let j = 0; j < timeslots; j++)
        {
            workingSchedule[j] = new Array(spaces);
        }

        for(let j in units)
        {
            AssignUnit(j);
        }

        //Evaluate
        Evaluate();
    }

    Present();
}

function AssignUnit(u : string)
{
    for(let l = 1; l < units[u].facilitators.length; l++) //check every facilitator
    {
        if(units[u].facilitators[l] == units[u].facilitators[0] && units[u].facilitators[0] != "NA") //if they're the same as 0 (the wrangler)
        {
            units[u].facilitators[l] = "NA"; //na them out
            //console.log("Same in " + units[u].code)
        }
    }
    
    //Find random empty cell
    let x = RandomRange(0, timeslots-1);
    let y = RandomRange(0, spaces-1);

    //if it is empty, assign this unit to it
    if(NullCheck(workingSchedule[x][y]))
    {
        workingSchedule[x][y] = units[u];
    }
    else
    {
        AssignUnit(u);
    }
}

//Judge how good the current working schedule is, replace best schedule if it's better
function Evaluate()
{
    //console.log("Evaluating");
    //for tracking how many clashes a facilitator has
    class facilitator
    {
        name: string;
        occurances: number;

        constructor(_name : string, _num : number)
        {
            this.name = _name;
            this.occurances = _num;
        }
    }

    let points : number = 10000;

    //Per timeslot
    for(let x = 0; x < timeslots; x++)
    {
        let facilitators: facilitator[] = new Array<facilitator>();
        let names : string[] = new Array<string>();

        //loop through units
        for(let y = 0; y < spaces; y++)
        {
            if(NullCheck(workingSchedule[x][y]))
            {    
                continue;
            }

            for(let i = 0; i < workingSchedule[x][y].facilitators.length; i++)
            {
                names.push(workingSchedule[x][y].facilitators[i]);
            }
            //console.log(workingSchedule[x][y].facilitators);

            names.sort();
            console.log(names);

            let count: number = 0;
            let current : string = "";

            for(let i = 0; i < names.length; i++)
            {
                //http://jsfiddle.net/aQsuP/9/, this is the biggest code difference from Unity C# implementation
                if(names[i] != current) //when we get to a new name
                {
                    //console.log(current);
                    if(count > 0) //if there were any results
                    {
                        //set up facilitator and add them to the array
                        facilitators.push(new facilitator(current, count));
                    }
                    current = names[i]; //set to the next name
                    count = 1;
                }
                else //we have another result of the same name
                {
                    count ++;
                }
            }
        }

        //console.log(workingSchedule[x]);
        //for all facilitators, if occurance > 1, subtract points * occurances (TODO: modified against priority and wrangler status)
        for(let i = 0; i < facilitators.length; i++)
        {
            if(facilitators[i] != null && facilitators[i].name != "NA" && facilitators[i].name != "")
            {
                if(facilitators[i].occurances > 1)
                {
                    points -= facilitators[i].occurances * 100;                             
                }
                //console.log(facilitators[i].name + facilitators[i].occurances.toString());  //Always comes back with multiple occurances regardless of timeslot
            }
        }
        //console.log(points); //Yep, indicating point deduction is working...
    }

    //if the points for this schedule are more than the best schedule, this is the best schedule
    if(points > bestPoints)
    {
        //console.log("UPDATED");
        //console.log(workingSchedule);
        bestPoints = points;
        bestSchedule = [...workingSchedule];
        //console.log(bestSchedule);
    }
}

//Spit out the result
function Present()
{
    let output : string = "<table class='table table-striped table-dark table-bordered'>";
    let clashString : string = ""
    //console.log(bestSchedule);
    //output bestSchedule
    for(let x = 0; x < timeslots; x++)
    {           
        output += "<tr><td>Timeslot: " + x.toString() + "</td>";

        for(let y = 0; y < spaces; y ++)
        {
            if(NullCheck(bestSchedule[x][y]))
            {    
                //output += "______" + " - "+ "</td><td>";
                continue;
            }
            else
            {
                output += "<td>" + bestSchedule[x][y].code + "</td>";
            }
        }
        output += "</td></tr>"
    }

    if(bestPoints >= 10000)
    {
        output += "<tr>"
    }
    else if(bestPoints > 5000 && bestPoints < 10000)
    {
        output += "<tr class='bg-warning'>"
    }
    else
    {
        output += "<tr class='bg-danger'>"
    }

    let clashNumber = (10000-bestPoints)/200;

    if(clashNumber == 1)
    {
        clashString = "1 Clash";
    }
    else
    {
        clashString = clashNumber.toString() + "Clashes";// + bestPoints.toString();
    }

    output += "<td colspan = '2'>" + clashString + "</td></tr>";
    document.getElementById("Results").innerHTML = output; //write output to the end
}







//Utility Functions
function RandomRange(min: number, max: number)
{
    return Math.floor(Math.random() * (max-min + 1) + min);
}

//More thorough check to see if a unit is valid, might be unnecessary?
function NullCheck(u : Unit)
{
    if(u == null)
    {
        return true;
    }
    if(u === null)
    {
        return true;
    }
    if(typeof u === 'undefined')
    {
        return true;
    }

    return false;
}

function AddUnit()
{
    let table : string = '<tr id="row' + unitNumber.toString() + '">';
    table += '<td><input type="text" id="Code' + unitNumber.toString() + '" value="AAA000"></td>';
    table += '<td><input type="text" id="Wrangler' + unitNumber.toString() + '" value="NA"></td>';
    table += '<td><input type="text" id="MEL' + unitNumber.toString() + '" value="NA"></td>';
    table += '<td><input type="text" id="SYD' + unitNumber.toString() + '" value="NA"></td>';
    table += '<td><input type="text" id="BRS' + unitNumber.toString() + '" value="NA"></td>';
    table += '<td><input type="text" id="PER' + unitNumber.toString() + '" value="NA"></td>';
    table += '<td><input type="text" id="BBY' + unitNumber.toString() + '" value="NA"></td>';
    table += '<td><input type="text" id="DUB' + unitNumber.toString() + '" value="NA"></td></tr>';
    document.getElementById("UnitsTable").innerHTML += table;
    unitNumber ++;
    //console.log(unitNumber);
}

function RemoveUnit()
{
    document.getElementById("row" + (unitNumber-1).toString()).remove();
    unitNumber--;
    //console.log(unitNumber);
}